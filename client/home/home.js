Template.home.rendered = function () {

  navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
  window.URL = window.URL || window.mozURL || window.webkitURL;

  var video = $("video")[0];

  navigator.getUserMedia({'video': true}, function (stream) {
    video.src = window.opera ? stream : window.URL.createObjectURL(stream);
    video.play();
  }, function(er) {
    console.error('Video capture error', er);
  });
};

Template.home.events({

  'click input': function(e){
    if(!Meteor.user()){
      return alert('please sign-in to to send a snap');
    }
  },

  'click .submit': function(e){
    
    if(Meteor.user()){

      var video = $('video')[0];
      var canvas = $('canvas')[0];
      var context = canvas.getContext('2d');
      var height = 240;
      var width = Math.floor(height * video.videoWidth/video.videoHeight);
      $('canvas').attr({width:width, height:height});
      context.drawImage(video, 0, 0, width, height);

        Snaps.insert({
          selfieSrc: canvas.toDataURL('image/jpeg'),
          to: $("#controls [type=email]").val(),
          from: Meteor.user().emails[0].address,
          time: 5,
          createdAt: Date.now()
        });
    }
  },
});

Template.home.helpers({
  snap: function(userId){
    Meteor.subscribe('selfies', userId);
    return Snaps.find();
  }
});